import { Component, OnInit } from '@angular/core';
import { Workout } from 'src/app/models/workout';
import { WorkoutService } from 'src/app/services/workout.service';

@Component({
  selector: 'app-workouts',
  templateUrl: './workouts.component.html',
  styleUrls: ['./workouts.component.scss'],
})
export class WorkoutsComponent implements OnInit {
  workouts: Workout[] | undefined;

  constructor(private workoutService: WorkoutService) {}

  ngOnInit(): void {
    this._getWorkouts();
  }

  private _getWorkouts(): void {
    this.workoutService.getAll().subscribe({
      next: (workouts) => {
        this.workouts = workouts;
      },
      error: (e) => {
        console.error(e);
      },
    });
  }
}
