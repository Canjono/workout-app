import { Component, OnInit } from '@angular/core';
import { Exercises, ExerciseCategory } from 'src/app/models/exercise';
import { ExerciseService } from 'src/app/services/exercise.service';

@Component({
  selector: 'app-exercises',
  templateUrl: './exercises.component.html',
  styleUrls: ['./exercises.component.scss'],
})
export class ExercisesComponent implements OnInit {
  exercises: Exercises = {
    legs: [],
    arms: [],
    abdominal: [],
    allRound: [],
  };

  activeTab: ExerciseCategory = ExerciseCategory.Legs;

  categoryName = ExerciseCategory;

  constructor(private exerciseService: ExerciseService) {}

  ngOnInit(): void {
    this._getExercises();
  }

  private _getExercises(): void {
    this.exerciseService.getAll().subscribe({
      next: (exercises: any) => {
        for (const exercise of exercises) {
          if (exercise.categories?.includes(ExerciseCategory.Legs)) {
            this.exercises.legs.push(exercise);
          }
          if (exercise.categories?.includes(ExerciseCategory.Arms)) {
            this.exercises.arms.push(exercise);
          }
          if (exercise.categories?.includes(ExerciseCategory.Abdominal)) {
            this.exercises.abdominal.push(exercise);
          }
          if (exercise.categories?.includes(ExerciseCategory.AllRound)) {
            this.exercises.allRound.push(exercise);
          }
        }
      },
    });
  }

  onSelectTab(category: string): void {
    switch (category) {
      case ExerciseCategory.Abdominal:
        this.activeTab = ExerciseCategory.Abdominal;
        break;
      case ExerciseCategory.AllRound:
        this.activeTab = ExerciseCategory.AllRound;
        break;
      case ExerciseCategory.Arms:
        this.activeTab = ExerciseCategory.Arms;
        break;
      case ExerciseCategory.Legs:
        this.activeTab = ExerciseCategory.Legs;
        break;
    }
  }
}
