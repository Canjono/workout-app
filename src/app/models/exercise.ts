export interface Exercise {
  id: string;
  name?: string;
  description?: string;
  categories?: ExerciseCategory[];
}

export interface Exercises {
  legs: Exercise[];
  arms: Exercise[];
  abdominal: Exercise[];
  allRound: Exercise[];
}

export enum ExerciseCategory {
  Legs = 'legs',
  Arms = 'arms',
  Abdominal = 'abs',
  AllRound = 'all-round',
}
