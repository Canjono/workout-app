import { Injectable } from '@angular/core';
import { initializeApp } from 'firebase/app';
import { Database, getDatabase, onValue, ref } from 'firebase/database';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class FirebaseService {
  private _database: Database;

  constructor() {
    const firebaseConfig = {
      apiKey: 'AIzaSyAkVWHNTDjINg4U3VV2p2_wxyl4OwU-u2E',
      authDomain: 'workout-f8969.firebaseapp.com',
      databaseURL:
        'https://workout-f8969-default-rtdb.europe-west1.firebasedatabase.app',
      projectId: 'workout-f8969',
      storageBucket: 'workout-f8969.appspot.com',
      messagingSenderId: '500810949542',
      appId: '1:500810949542:web:311ad699a899cfce55e567',
      measurementId: 'G-Y9LK5948BX',
    };

    // Initialize Firebase
    const app = initializeApp(firebaseConfig);
    this._database = getDatabase(app);
  }

  getExercises(): Observable<any> {
    return new Observable((subscriber) => {
      const reference = ref(this._database, 'exercises');
      onValue(reference, (snapshot) => {
        subscriber.next(snapshot.val());
        subscriber.complete();
      });
    });
  }

  getWorkouts(): Observable<any> {
    return new Observable((subscriber) => {
      const reference = ref(this._database, 'workouts');
      onValue(reference, (snapshot) => {
        subscriber.next(snapshot.val());
        subscriber.complete();
      });
    });
  }
}
