import { Injectable } from '@angular/core';
import { forkJoin, map, Observable } from 'rxjs';
import { Exercise } from '../models/exercise';
import { Workout } from '../models/workout';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root',
})
export class WorkoutService {
  constructor(private firebaseService: FirebaseService) {}

  getAll(): Observable<Workout[]> {
    const observables = {
      workouts: this.firebaseService.getWorkouts(),
      exercises: this.firebaseService.getExercises(),
    };

    return forkJoin(observables).pipe(
      map((result) => {
        result.workouts.forEach((workout: any) =>
          this._mapExercises(workout, result.exercises)
        );
        return result.workouts;
      })
    );
  }

  private _mapExercises(workout: Workout, exercises: Exercise[]): void {
    for (let i = 0; i < workout.exercises.length; i++) {
      const exercise = workout.exercises[i];
      const foundExercise = exercises.find((x) => x.id === exercise.id);

      if (foundExercise) {
        workout.exercises[i] = foundExercise;
      }
    }
  }
}
