import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { FirebaseService } from './firebase.service';

@Injectable({
  providedIn: 'root',
})
export class ExerciseService {
  constructor(private firebaseService: FirebaseService) {}

  getAll(): Observable<any> {
    return this.firebaseService.getExercises();
  }
}
